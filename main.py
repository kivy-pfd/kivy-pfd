from kivy.app import App

#from kivy.uix.scatter import Scatter
from kivy.graphics.transformation import Matrix
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.floatlayout import FloatLayout
#from kivy.uix.textinput import TextInput
#from kivy.uix.boxlayout import BoxLayout
from kivy.clock import Clock
#from FlightGear import FlightGear
#from kivy.uix.popup import Popup
#from kivy.config import Config
from kivy.properties import NumericProperty
import time
import sys
import errno
from socket import *
   
host = ""
outputline = "foo"
port = 9009
in_port = 9010
telnet_port = 9000
#buf = 1024
addr = (host,port)
UDPSock = socket(AF_INET,SOCK_DGRAM)
insock =  socket(AF_INET,SOCK_DGRAM)
insock.setblocking(0)
max_view=10      
fg = False
speed_y=-200
bank =0
tilt =-460


try:
    host = Config.get('kivy', 'flightgear_ip')
except:
    print "no flightgear_ip entry in config.ini"
    host= 'localhost'
    

    
def my_update(dt):
    global speed_y
    global bank
    global tilt
    speed_y+=1
    bank-=0.5
    if tilt < 0:
        tilt+=1
    else:
        tilt =-460
    pfd.ids.speedtape.y = speed_y
    pfd.ids.horizon.y = tilt
    #m = Matrix()
    #m.rotate(1,0,0,1)
    #pfd.ids.horizon.apply_transform(trans=m,post_multiply=False)
    pfd.ids.horizon.angle = bank
    
class RotatedImage(Image):
    angle = NumericProperty()
    
    
class MyPfd(FloatLayout):
    
    pass

class Pfd(App):
    def build(self):
        global pfd
        
        #crudeclock = IncrediblyCrudeClock()
        #popup = Popup(title='Test popup', content=Label(text='Hello world'),auto_dismiss=False)
       
        pfd = MyPfd()
        #popup.open()
        #insock.bind(('', in_port)) 
        #print "linstening on port ", in_port
        #read_udp()
        Clock.schedule_interval(my_update, 0.1)
           
        return pfd

if __name__ == "__main__":
    Pfd().run()

